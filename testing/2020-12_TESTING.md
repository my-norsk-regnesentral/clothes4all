# Testing in December of 2020

## 1.2 Memory

Tested on 2020-12-14, kl.11:45.
Simulation strategy: Making a mistake and trying to start over.

Task 13: 
* without rule: 
I cannot complete the task because I cannot choose Large. The total price is $218 dollar or 218 NOK for a small size. 
Time: ca 02:00:00.
Satisfactions: 4. 
* with rules: The total price is 1,148 NOK.
Time: ca 00:50:00.
Satisfaction: 6.

## 1.4 Reasoning

Tested on 2020-12-17, kl.16:50
Simulation strategy: Web Disability Simulator (small vocabulary)

Task 29
* without rules
Could not solve this task with the chosen simulation strategy. I went to About, and I see some links. But I cannot read the information about the links or the links themselves.
Time: 02:10:00
Satisfaction: 1
* with rules: Could not solve because of the same reasons as before, but at least the text is better readable now. I can read something about Family, IT, and Center.
Time: 01:15:00
Satisfaction: 2

## 1.6 Linguistics and speech

Tested on 2020-12-14, kl.12:05
Simulation strategy: Web Disability Simulator: dyslexia

Task 27:
* without rules: The page does not exist.
Time: ca 02:40:00
Satisfaction: 0.
* with rules: The page does not exist.
Time: ca 02:40:00
Satisfaction: 0.

Tested on 2020-12-14, kl.12:25
Simulation strategy: Web Disability Simulator: dyslexia

Task 28:
* without rules: The page does not exist.
Time: ca. 01:15:00
Satisfaction: 0
* with rules: The page does not exist.
Time: ca. 01:20:00
Satisfaction: 0

Tested on 2020-12-17, kl.09:55
Simulation strategy: Dyslexia Simulation (plugin)

Task 27:
* without rules: 
Bring the receipt. You can return at a Clothes4All shop or send it back. It takes up to 10 days.
Time: ca 02:15:00
Satisfaction: 1.
Had to type in the return page manually.
* with rules:
Bring the receipt. You can get to Clothes4All shop or send it back to them by mail. It takes 10 days.
Time: ca 03:50:00
Satisfaction: 3.
Had to type in the return page manually.

## 1.8 Mental health and behavior

Tested on 2020-12-14, kl.12:00.
Simulation strategy: Act as if you are using the website for the first time, as if I got distracted.

Task 26:
* without rule: The shipping costs 199 NOK.
Time: ca 00:55:00.
Satisfaction: 4. (low contrast)
* with rule: The shipping costs 199 NOK.
Time: ca 00:30:00.
Satisfaction: 7.

## 2.1.2 Low vision

Tested on 2020-12-18, kl.13:25
Simulation strategy: Wearing four Cambridge simulation glasses

Task 18
* without rules: There are 6 dresses, 4 shorts, and 7 pants.
Time: 01:55:00
Satisfaction: 4. could not see the text because of the low contrast.
* with rules: There are 6 dresses, 4 shorts, and 7 pants.
Time: 01:20:00
Satisfaction: 6. It was easier to see the text now. But links should be better highlighted.

## 2.1.3 Reduced color vision

Tested on 2020-12-17, kl.10:40
Simulation strategy: No coffee Vision Simulator: Deuteranopia Color Vision Deficiency.

Task 20:
* without rules:
I don't know. I guess not. *shrug*
Time: 01:50:00.
Satisfaction: 3
* with rules:
I don't know. I guess not.
Time: 01:20:00
Satisfaction: 2

Task 21:
* without rules:
Yes, there is a woolen coat that is green.
Time: 00:55:00
Satisfaction: 7
I read the descriptions though.
Time:
* with rules:
Yes, there is a woolen coat that is green. It costs 89 USD.
Time:
01:20:00
Satisfaction: 6
I had to read the descriptions though.

## 2.2.2 Limited hearing

Tested on 2020-12-17, kl.16:35
Simulation strategy: Lowered volume on headphones to 2. Lots of background noises.

Task 5
* without rules: Not possible to solve with volume at 2. I increased to 10, and I could barely hear it. The suit is linen and fuzzy. The materials are exotic. 
Time: 03:40:00
Satisfaction: 1
* with rules I could solve it with volume at 2. I was using the captions and/or the text version. The suit is linen, and the materials are exotic.
Time: 02:00:00
Satisfaction: 8

## 3.1 Lack of voice

Tested on 2020-12-17, kl.16:55
Simulation strategy: Turning off my microphone.

Task 9
* without rules: There are five suits, three blazers, and five shirts.
Time: 01:25:00
Satisfaction: 7. Fair enough.
* with rules: There are 5 suits, 3 blazers, and 5 shirts.
Time: 01:00:00
Satisfaction: 8. Fair enough.

## 4.1.1 Limited mobility reach

Tested on 2020-12-18, kl.13:55.
Simulation strategy: Simulating being fixed against the chair, with elbows to the sides and using the keyboard.

Task 
* without rules: There are 23 fashion items in total.
Time: 01:45:00
Satisfaction: 7. I could not select the drop-down menu.
* with rules: There are 23 fashion items in total.
Time: 01:10:00
Satisfaction: 9. The menu was much more accessible.

## 4.2.2 Limited motor strength and/or manipulation

Tested on 2020-12-18, kl.14:05
Simulation strategy: Using simulation gloves and keyboard.

Task 
* without rules: It costs 328 USD
Time: 01:20:00
Satisfaction: 8. Fair enough
* with rules: It costs 328 USD.
Time: 01:10::00
Satisfaction: 9. Awesome keyboard navigation.					
