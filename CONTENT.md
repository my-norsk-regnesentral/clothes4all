# Content 

[Home](./README.md)

Vi skal lage video content som skal publiseres i en tilgjengelig og i en utilgjengelig versjon på nettsiden.
Disse videoene skal ha musikk og narrasjon.
I første omgang fokuserer vi på disse fire filmene:

|||
|:-:|:-|
|[![Mann med mobiltelefon i finansdistriktet](./media/bilder/business-smart-02_thumb.png)](./media/filmer/business-smart-01.mp4)|[![Kvinne med kamera i blomsteråker](./media/bilder/summer-casual-01_thumb.png)](./media/filmer/summer-casual-01.mp4)|
|[Mann i finansdistriktet](#mann-i-finansdistriktet)|[Kvinne i blomsteråker](#kvinne-i-blomsteråker)|
|[![Mann i hvit dress på stranda](./media/bilder/beach-smart-02_thumb.png)](./media/filmer/beach-smart-01.mp4)|[![Stillig kvinne med solbriller i byen](./media/bilder/city-stylish-01_thumb.png)](./media/filmer/city-stylish-01.mp4)|
|[Mann på stranda](#mann-på-stranda)|[Stilig kvinne i byen](#stilig-kvinne-i-byen)|

## Mann i finansdistriktet

[Up](#content)

__EN:__   
♫♪ Epic piano music ♫♪

A man walking in front of modern business architecture with columns.
He is wearing fine, brown leather shoes, 
slim blue jeans, 
a black cotton shirt, 
and a tan wool overcoat.

♫♪ Music intensifies with strings ♫♪

This man's overcoat made of disposable materials 
presents a classic, clean look 
that says "smart and luxury",
whether traveling to a business meeting or a formal occasion.

♫♪ Dramatic string and piano music ♫♪

__NO:__  
♫♪ Majestetisk piano musikk ♫♪

En herre går blant moderne kontorbygninger med søyler.
Han har på seg fine, brune lærsko, 
en slim fit jeans, 
en sort bomullsskjorte, 
og en beige frakk laget av ull.

♫♪ Musikken dramatiseres med strykere ♫♪

Med denne lekre, klassiske frakken 
som er for engangsbruk, 
kan du heve standarden 
både på fritiden og på jobb.

♫♪ Piano med strykere ♫♪

[Epic Cinematic](./media/lydfiler/scott-holmes_epic-cinematic) | 
[Kilde](https://freemusicarchive.org/music/Scott_Holmes/Cinematic_Background_Music/-_Epic_Cinematic)

Copyright Video: (By Scott Holmes under CC BY-NC)

## Kvinne i blomsteråker

[Up](#content)

Halv speed.

__EN:__  
♫♪ Joyful guitar music ♫♪

A young woman carrying a camera 
is running through a hilly field of rapeseed towards the horizon.
She is wearing a white sleeveless top, jeans shorts, and suspenders.

Fun and flirty cotton materials 
make this boho outfit made of sustainable materials 
a first choice for her next summer music festival adventure.

The sky is clear with some pinkish clouds from the sunset.

♫♪ Joyful guitar music ♫♪

__NO:__  
♫♪ Gledelig gitarmusikk ♫♪

En ung kvinne med kamera løper gjennom en rapsblomstereng mot horisonten.
Himmelen er klar med noen skyer med rosa preg av solnedgangen.

Få denne lette og flørtende looken 
med en hvit ermeløs topp, olashorts og bukseseler. 
Alle plaggene er laget av bærekraftige materialer 
som er et godt valg for varme sommerdager.

♫♪ Gledelig gitarmusikk ♫♪

[Happiness](./media/lydfiler/bensound-hapiness.mp3) | 
[Kilde](https://www.bensound.com/royalty-free-music/track/happiness)

## Mann på stranda

[Up](#content)

Halv speed.

__EN:__  
♫♪ Relaxing upbeat music ♫♪

A man walks on the beach with a white suit and brown belt.
He is wearing a dark-blue shirt under the open suit jacket.
There are people and seagulls bathing and frolicing on the beach.

Be ready for an unforgettable evening at the beach 
with this linen no fuss suit. 
A Hawaiian shirt made of exotic materials
provides an enhanced summer feel.

♫♪ Relaxing upbeat music ♫♪

__NO:__    
♫♪ Avslappende munter musikk ♫♪

En mann står på stranda i en hvit dress med brunt belte.
Under den åpne dressjakken har han en mørkeblå skjorte.
Det er folk og måker i bakgrunnen som bader og leker på stranden.

Kombiner en stilig dressjakke til et par linbukser, 
og ha gjerne en Hawaii-skjorte laget av eksotiske materialer under 
for å gjøre looken klar for en sommerfest!

♫♪ Avslappende munter musikk ♫♪

[Summer Beach Music](./media/lydfiler/bensound-summer.mp3) | 
[Kilde](https://www.bensound.com/royalty-free-music/track/summer-chill-relaxed-tropical)

Copyright Video: Attribute Bensound in the descriptions.

## Stilig kvinne i byen

[Up](#content)

__EN:__  
♫♪ Upbeat electronic music ♫♪

A woman walks in a busy street.
She is wearing big, square earrings, 
piercings in the ear and nose, 
a striped cotton shirt, 
and a cream colored polyester trenchcoat.

Show poise and power in the boardroom 
with this suit made of locally sourced materials.

She puts on some slim, red shades.

And break barriers with an ensemble 
that combines classic style 
with modern accessories. 

♫♪ Upbeat electronic music ♫♪

__NO:__  
♫♪ Livlig elektronisk musikk ♫♪

En dame går i en travel gate.
Hun har på seg store firkantete øreringer, 
piercinger i øret og nesa, 
en stripete bomullsskjorte, 
og en kremfarget polyesterkåpe.
Hun tar på seg noen smale, 
røde solbriller.

Imponer med moderne, oppsiktsvekkende tilbehør 
til denne tidløse sammensetningen av en klassisk kåpe og skjorte 
laget av lokale materialer som kler din fasong.

♫♪ Livlig elektronisk musikk ♫♪

[Super Techno](./media/lydfiler/jonah-rapino_super-techno.mp3) |
[Kilde](https://freemusicarchive.org/music/Jonah_Rapino/Adventures_In_Flying_Soundtrack/Super_Techno_1129)

[Technomagus City (ID 501)](./media/lydfiler/lobo-loco_technomagus-city.mp3) | 
[Kilde](https://freemusicarchive.org/music/Lobo_Loco/Space_Prophecy/Technomagus_City_ID_501)

Copyright Video Video: (CC BY-NC-ND 3.0) | (CC BY-NC-ND 4.0)
