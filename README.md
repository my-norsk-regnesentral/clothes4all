# Clothes4All - Administration

Dette dokumentet inneholder oversikt over administrative oppgaver i [Clothes4All prosjektet](https://drive.google.com/drive/folders/185cgG6MZkb9ZIftx4LGI8s5rJJz2wTFi).
Vi skal videreføre referansenettstedet fra i fjor ved å utvide nettstedet med et komplett sett av regler som matcher WCAG 2.1 på nivå AAA (muligens med unntak av 2.3.1). 
Nettsiden kan brukes for læring, demonstrering, validering, bevisstgjøring/empatiskaping og som referanse.
Hovedmålet i høst 2020 er å koordinere utvikling og koodinering av rammeverket for testing beskrevet i [`Arbeidspakke 3: Testing`](https://docs.google.com/document/d/1ogJm67oO22qcJzxcoAOjpOxMPyQLUJuJONtgP_L_nYs/edit).
1. Den første milepælen i denne arbeidspakken er et [rammeverk](https://docs.google.com/document/d/1tzQULGDkwMCPUAGFaFABQOx4UhmbXb7OFEcJJu4uia4/edit#) som:
   * definerer og beskriver _funskjonsnedsettelser_ , 
   * presenterere _simuleringsmetoder og -redskap_ for disse funskjonsnedsettelsene, 
   * beskriver og instruerer konkrete _brukersentrerte målingsmetoder_ for tilhjengelighet og brukervennlighet av nettsiden,
2. Den andre milepælen er en validering av rammeverket med scåring for Clothes4All før og etter tilgjengelighetsforbedringen for ulike funskjonsnedsettelsessimuleringer. 

# Oversikt

Oversikt over utstående oppgaver finner du [her](https://gitlab.com/nr-norsk-regnesentral/clothes4all/-/boards).
Milepælene står [her](https://gitlab.com/nr-norsk-regnesentral/clothes4all/-/milestones).

* [Video content](./CONTENT.md)
* Referater
   * [Møter i (09) September 2020](./referater/2020-09_MOTER.md)
